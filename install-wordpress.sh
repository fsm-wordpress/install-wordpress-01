#!/bin/bash

apt update -y && apt-get install -y git

curl -fsSL https://get.docker.com -o get-docker.sh

sh get-docker.sh

#sudo usermod -aG docker your-user

docker swarm init

git clone https://gitlab.com/fsm-wordpress/install-wordpress-01

cd install-wordpress-01/


docker stack deploy -c wordpress.yml site